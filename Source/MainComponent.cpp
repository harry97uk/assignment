/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback(String(), this);
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
    
    
    midiLabel.setColour(Label::textColourId, Colours::black);
    midiLabel.setText("MIDI", dontSendNotification);
    addAndMakeVisible(midiLabel);
    
    
    Velocity.setSliderStyle(Slider::IncDecButtons);
    Velocity.setRange(0, 127, 0.1);
    addAndMakeVisible(Velocity);
    Velocity.addListener(this);

    ChannelSlider.setSliderStyle(Slider::IncDecButtons);
    ChannelSlider.setRange(1, 16, 1);
    addAndMakeVisible(ChannelSlider);
    ChannelSlider.addListener(this);
    
    Number.setSliderStyle(Slider::IncDecButtons);
    Number.setRange(0, 127, 1);
    addAndMakeVisible(Number);
    Number.addListener(this);
    
    play.setButtonText("Play");
    addAndMakeVisible(play);
    play.addListener(this);
    
    
    
    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
    midiLabel.setBounds(10, 10, getWidth() - 20, 50);
    ChannelSlider   .setBounds(10, 70, getWidth() - 20, 20);
    Number.setBounds(10, 100, getWidth() - 20, 20);
    Velocity.setBounds(10, 130, getWidth() - 20, 20);
    play.setBounds(10, 180, getWidth() - 20, 20);
}

void MainComponent::handleIncomingMidiMessage(MidiInput* midiInput, const MidiMessage& message)
{
    DBG("Received");
    
    String midiText;
    
    if (message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << "\nNumber: " << message.getNoteNumber();
        midiText << "\nVelocity: " << message.getVelocity();
        
    }
    midiLabel.getTextValue() = midiText;
    
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider==&ChannelSlider) {
        midiMsg.setChannel(ChannelSlider.getValue());
    }
    else if (slider==&Number) {
        midiMsg.setNoteNumber(Number.getValue());
    }
    else if (slider==&Velocity) {
        midiMsg.setVelocity(Velocity.getValue());
    }
}

void MainComponent::buttonClicked(Button* button)
{
    if (button == &play) {
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(midiMsg);
    }
}
